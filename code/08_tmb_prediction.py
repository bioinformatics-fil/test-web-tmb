import requests
import zipfile
import tarfile
import pandas as pd
import csv
import os

os.chdir('..\\data')
#%%
# =============================================================================
# extraer muestras comprimidas
#  # # se extraen en DATA ya que es el directorio en el que se encuentra
# =============================================================================

with zipfile.ZipFile('09_samples.zip') as zmuestras:
    zmuestras.extractall()

#%%
# =============================================================================
# Calcular prediccion TMB a traves de RestAPi en TMBpred
# =============================================================================
lista_tumor = ["BRE", "COL", "END", "GAS", "GLI", "HAV", "LIV", "MEL", "NME", "NSL", "PAN", "PRO", "UNI"]
jobids = []

for tumores in lista_tumor:     
    pred_tmb = "http://tmbpred.leloir.org.ar/api/pred_coordenates"
    data_send = {
        "model-cancer": tumores, 
        "model-quality": "B", 
        "model-databy": ["G"], 
        "model-subset": ["O"], 
        "email": "", 
        "efile-delimiter": "\t", 
        "efile-header": True, 
        "column-chromosome": 1, 
        "column-start_position": 2, 
        "column-end_position": 3
        }
    file_send = {
        "efile-filename": (f'{tumores}.zip', open(f'{tumores}.zip',"rb"), "application/x-zip-compressed") 
        }       
    consult_r = requests.post(pred_tmb, data=data_send, files=file_send)
    output_json = consult_r.json()
    print(output_json)
    jobids.append(output_json['job_id'])# lista con todos los job id para seguir trabajando despues, no se muestra la extensión
    print(jobids) 

#%%
# =============================================================================
# Descargar Resultados
# # # se descargan los resultados en la carpeta DATA, que es el directorio actual
# =============================================================================

for jobid in jobids:
    down_data = "http://tmbpred.leloir.org.ar/download/" + jobid;         
    consult_r = requests.get(down_data)
    if consult_r.status_code == 200:
        #save as tar.gz     
        with open(jobid + ".tar.gz", "wb") as f:
            f.write(consult_r.content)    
    else:
        #some error found
        output_json = consult_r.json()
        print(output_json)

#%%
# =============================================================================
# Extraer datos de prediccion por tipo de cancer
# # # se crean las carpetas en la carpeta DATA, que es el directorio actual
# # # a la lista de jobids les agrego la extension tar.gz
# =============================================================================
jobids_tar = ['788c913b-2f8c-4597-966c-cfb7a9dc4ffb.tar.gz', 'e62ec980-70fe-423e-90e3-0e207fee96ff.tar.gz', '7e9fb574-de81-48c1-ae48-d02470caf9ce.tar.gz', 'b3f631df-b180-4bb9-8f8c-e1f7567407d2.tar.gz', '65262123-001d-4831-b390-75cf02add44d.tar.gz', '86aeb99b-5e5f-48cf-a8ec-52945348d671.tar.gz', '27cff0fd-79b1-4d7d-98a2-4bee97dce606.tar.gz', '98721d26-bed5-4fb3-93cd-8202514c1e7f.tar.gz', 'ed57246f-8536-4518-a7fa-ddff35cfe314.tar.gz', '608504f5-9511-44ad-b50a-4def7b99c6c3.tar.gz', 'e4601acc-3525-46c9-9289-13881cad1d46.tar.gz', '4a37a2e1-ee4d-4fc5-9c09-04c126b8ee6e.tar.gz', 'de1215e3-d838-464e-830c-105cebc5295d.tar.gz'] 

for jobid in jobids_tar: 
    tar_job = tarfile.open(jobid, 'r:gz')
    for member in tar_job.getmembers():
        if "predicted_TMB.txt" in member.name:
            tar_job.extract(member)# devuelve una carpeta con el nombre del job y dentro el archivo predicted_TMB.txt


# %%
# =============================================================================
# Generar tabla de tmb predichos para todos los tipos de cancer con muestras. 
#  # # columnas: sample , cancer_type , tmb_predicho
#  # # nombre del archivo final tiene que ser : 08_tmbpredicho_TMBpred.txt
# =============================================================================
jobids = ['788c913b-2f8c-4597-966c-cfb7a9dc4ffb', 'e62ec980-70fe-423e-90e3-0e207fee96ff', '7e9fb574-de81-48c1-ae48-d02470caf9ce', 'b3f631df-b180-4bb9-8f8c-e1f7567407d2', '65262123-001d-4831-b390-75cf02add44d', '86aeb99b-5e5f-48cf-a8ec-52945348d671', '27cff0fd-79b1-4d7d-98a2-4bee97dce606', '98721d26-bed5-4fb3-93cd-8202514c1e7f', 'ed57246f-8536-4518-a7fa-ddff35cfe314', '608504f5-9511-44ad-b50a-4def7b99c6c3', 'e4601acc-3525-46c9-9289-13881cad1d46', '4a37a2e1-ee4d-4fc5-9c09-04c126b8ee6e', 'de1215e3-d838-464e-830c-105cebc5295d'] 
lista_df = []  

for jobid in jobids: # recorro las carpetas 
    cancer = pd.read_csv(f'{jobid}/predicted_TMB.txt', sep = '\t') # leo el archivo txt con pandas
    df= pd.DataFrame(columns=['sample','cancer_type','tmb_predicho']) # genero un nuevo df con las columnas que quiero
    df['sample'] = cancer['sample_id']
    df['cancer_type'] = cancer['cancer_id']
    df['tmb_predicho'] = cancer['predicted_TMB'] # extraigo las columnas importantes de todos los archivos y las agrego a df. Uno por tipo de cancer
    lista_df.append(df) # genero una lista de dataframes [[df1],[df2],[df3]...]


#concateno todos los dataframe para tener uno unico con todos los tipos de cancer
tmb_prediccion_df = pd.concat([lista_df[0],lista_df[1],lista_df[2],lista_df[3],lista_df[4],lista_df[5],lista_df[6],lista_df[7],lista_df[8],lista_df[9],lista_df[10],lista_df[11],lista_df[12]])
tmb_prediccion_df = tmb_prediccion_df.set_index('sample')

# lo guardo en un txt
tmb_prediccion_df.to_csv('08_tmbpredicho_TMBpred.txt', sep="\t", quoting=csv.QUOTE_NONE, escapechar=" ")

# %%
