#%%
import io , os
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
from scipy import stats as sp
from sklearn import metrics 
import csv

os.chdir('..\\data')

#%%

#abro los txt
with open('08_tmbpredicho_TMBpred.txt') as file:
    data1 = io.StringIO(file.read().strip())

with open('08_tmbexperimental_cosmic.txt') as file:
    data2 = io.StringIO(file.read().strip())

df_pred = pd.read_csv(data1, sep="\t") 
df_exp = pd.read_csv(data2, sep="\t")

#%%
# =============================================================================
# Analisis exploratorio
# boxplot, eje x cada tipo de cáncer , eje y el tmb experimental 
# =============================================================================
cancer= ["BRE", "COL", "END", "GAS", "GLI", "HAV", "LIV", "MEL", "NME", "NSL", "PAN", "PRO", "UNI"]

datos_boxplot= [] #cada sublista es un tipo de cancer y contiene los datos del TMB experimental
for i in range(len(cancer)):
    info = df_exp[df_exp["cancer_type"] == cancer[i]]
    info = info["tmb_experimental"].tolist()
    datos_boxplot.append(info)

plt.figure(figsize=(10,7))
plt.ylabel("Experimental TMB (log scale)",fontsize=17)
plt.xlabel("Cancer types",fontsize=17)
plt.axhline(y=150, color='b', linestyle='--',lw=1.5 , label= "Threshold"+ "\n" + "value suggested for medical use"+ "\n" + "150 mutations")
plt.boxplot(datos_boxplot,showfliers=False)
plt.xticks(range(1,len(cancer)+1),cancer)
plt.yscale("log")
plt.legend(loc="best")
plt.tight_layout()
plt.savefig("09_boxplot_testing_website.png", bbox_inches='tight')


#%%
# =============================================================================
# 2)grafico por cada cancer mostrando en eje de las x el tmb experimental y eje y el tmb predicho y utilizar el coeficiente de correlación de Spearman para ajustar.
# =============================================================================

# agrupo en un unico df las dos columnas (experimental y predicho)
df_exp_pred = pd.concat([df_pred, df_exp['tmb_experimental']], axis =1) # agrupo en un unico df las dos columnas (experimental y predicho)

df_exp_pred.rename(columns={'tmb_predicho':'Predicted TMB',
                        'tmb_experimental':'Experimental TMB',
                        'cancer_type':'Cancer'},
                    inplace=True)

#grafico multiples graficos por tipo de cancer 
g = sns.lmplot(x='Experimental TMB', y='Predicted TMB', data=df_exp_pred,col='Cancer', hue="Cancer",col_wrap=5, ci=None, palette="muted", height=3,scatter_kws={"s": 10, "alpha": 1}, facet_kws={'sharey': False, 'sharex': False})
g.set(ylim=(0, None), xlim=(0,None))


#para mostrar el coeficiente de correlacion de spearman y el p valor
def annotate(data, **kws):
    r, p = sp.stats.spearmanr(data['Experimental TMB'], data['Predicted TMB'])
    ax = plt.gca()
    ax.text(.05, .9,'Spearman rank correlation',transform=ax.transAxes)
    ax.text(.05, .8, 'r={:.2f}'.format(r),transform=ax.transAxes)
    ax.text(.05, .7, 'p={:.2g}'.format(p),transform=ax.transAxes)
g.map_dataframe(annotate)
plt.savefig("09_regresionlineal_testing_website.png", bbox_inches='tight')

#%%
# =============================================================================
# Evaluar las muestras segun la clasificación de alto y bajo tmb
# ==================================================================
df = df_exp_pred
# 0 = bajo TMB
# 1 = alto TMB

df.loc[df['Experimental TMB'] < 150 , 'Experimental TMB'] = 0

df.loc[df['Experimental TMB'] >= 150, 'Experimental TMB'] = 1

df.loc[df['Predicted TMB'] < 150, 'Predicted TMB'] = 0

df.loc[df['Predicted TMB'] >= 150, 'Predicted TMB'] = 1

# %%
# =============================================================================
# Matriz confusion y metricas asociadas
# =============================================================================

# Confusion Matrix
matrices_confusion = []

for i,grupo in df.groupby("Cancer"): # los i son los tipo de cancer
    df2= df.loc[df['Cancer'] == i]
    matriz_confusion = metrics.confusion_matrix(df2["Experimental TMB"],df2["Predicted TMB"])
    df2=df
    matrices_confusion.append(matriz_confusion)

metricas = [] # contiene todas las metricas para los tipos de cancer evaluados

for i,grupo in df.groupby("Cancer"): # los i son los tipo de cancer
    #print(grupo.count()) # para saber cuantas muestras hay de cada tipo de cancer
    df2= df.loc[df['Cancer'] == i]
    # Accuracy
    exactitud= metrics.accuracy_score(df2["Experimental TMB"],df2["Predicted TMB"])
    metricas.append(exactitud)
    # print("exactitud:",exactitud)
    # Recall
    sensibilidad= metrics.recall_score(df2["Experimental TMB"],df2["Predicted TMB"])
    metricas.append(sensibilidad)
    # print("sensibilidad:",sensibilidad)
    # Precision
    precision= metrics.precision_score(df2["Experimental TMB"],df2["Predicted TMB"])
    metricas.append(precision)
    # print("precision:",precision)
    # Specificity
    especificidad = metrics.recall_score(df2["Experimental TMB"],df2["Predicted TMB"],pos_label=0)
    metricas.append(especificidad)
    # print("Especificidad:",especificidad)
    # f1 score
    f1_score = metrics.f1_score(df2["Experimental TMB"],df2["Predicted TMB"])
    metricas.append(f1_score)
    # print("fi_score:",f1_score)
    df2=df

# %%
# =============================================================================
# Tabla final metricas
# =============================================================================

#hago un lista de listas para tener las metricas por tipo de cancer
output=[metricas[i:i + 5] for i in range(0, len(metricas), 5)]

#creo una tabla final con los resultados de las metricas 
mt1= pd.DataFrame(columns=['Tumor', 'Samples'] )
mt1['Tumor'] = ["BRE", "COL", "END", "GAS", "GLI", "HAV", "LIV", "MEL", "NME", "NSL", "PAN", "PRO", "UNI"]
mt1['Samples'] = [159,368,42,72,237,119,7,389,68,84,140,14,17]

mt2 =pd.DataFrame(output, columns=['Accuracy','Recall','Precision','Specificity','F1'])

mt3 = pd.concat([mt1, mt2], axis=1).round(3)

# lo guardo en un txt
mt3.to_csv('09_metrics_table.txt', sep="\t", quoting=csv.QUOTE_NONE, escapechar=" ")

# %%
