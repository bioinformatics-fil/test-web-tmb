open(COSMIC, "../data/00_v95_CosmicGenomeScreensMutantExport.tsv");
open(COSMIC_selected, ">../data/01-mutation_v95.tsv");
#Gene name	Accession Number	Gene CDS length	HGNC ID	Sample name	ID_sample	ID_tumour	Primary site	Site subtype 1	Site subtype 2	Site subtype 3	Primary histology	Histology subtype 1	Histology subtype 2	Histology subtype 3	Genome-wide screen	MUTATION_ID	GENOMIC_MUTATION_ID	LEGACY_MUTATION_ID	Mutation CDS	Mutation AA	Mutation Description	Mutation zygosity	LOH	GRCh	Mutation genome positionMutation strand	FATHMM prediction	FATHMM score	Mutation somatic statusPubmed_PMID	ID_STUDY	Sample Type	Tumour origin	Age	HGVSP	HGVSC	HGVSG

while( $linea = <COSMIC> ) {
	my @cols = split(/\t/, $linea);
	my $tt = $cols[0];	
	#if (($cols[20] ne "p.?") and ( $tt !~ /_ENST/)){
	if ( $tt !~ /_ENST/){
		splice @cols, 31, 4;
		splice @cols, 22, 8;
		splice @cols, 15, 6 ;
		splice @cols, 6, 1;
		splice @cols, 1, 4;
		my $new_line = join("\t", @cols);
		print COSMIC_selected $new_line;
	}
}

close COSMIC;
close COSMIC_selected;
exit;
